JFrog CLI
=========

Docker_ image to run `JFrog CLI <https://www.jfrog.com/confluence/display/CLI/JFrog+CLI#JFrogCLI-WelcometoJFrogCLI/>`_,
a client that provides a simple interface that automates access to JFrog products (including Artifactory).

.. _Docker: https://www.docker.com
