FROM centos:7

LABEL maintainer "benjamin.bertrand@esss.se"

RUN yum install -y \
      make && \
    yum clean all \
  && rm -rf /var/cache/yum

ENV JFROG_VERSION 1.20.2

RUN curl -XGET "https://api.bintray.com/content/jfrog/jfrog-cli-go/${JFROG_VERSION}/jfrog-cli-linux-amd64/jfrog?bt_package=jfrog-cli-linux-amd64" -L -k  > /usr/local/bin/jfrog \
  && chmod a+x /usr/local/bin/jfrog \
  && useradd -m -s /bin/bash -g users csi

USER csi

# install default configuration with artifactory url
RUN mkdir -p /home/csi/.jfrog
COPY jfrog-cli.conf /home/csi/.jfrog/jfrog-cli.conf
